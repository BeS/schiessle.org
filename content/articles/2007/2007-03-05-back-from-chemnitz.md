---
title: Back from Chemnitz
author: admin
type: post
date: 2007-03-05T20:51:07+00:00
slug: back-from-chemnitz
categories:
  - english
tags:
  - fsfe

---
<img src="/img/articles/clt07_fsfe_booth.png" class="article-image" style="float: right;" />
At this weekend Rainer and I were in Chemnitz at the [&#8220;Chemnitzer Linux Tage&#8221; (CLT)][1]. It was the first time that FSFE had a booth at this event and for me it was the first time going to a fair especially to work at the FSFE booth.

The first day was really hard for us. Because of the late decision to go to Chemnitz we had a bad place for our booth and many visitors hadn&#8217;t seen us. But luckily we had found a much better place for the second day. This is a picture of our booth at the second day, it was in the central corridor in front of the entrance:

I was surprised how many visitors come to our booth and asked who we are and what we are doing. After they had seen our GPLv3 stickers and pins the new version of the GPL and especially the DRM clause was a common topic, too. It was a lot of fun to talk about the FSFE, our work and other related topics to the visitors.


  
Now that I&#8217;m back from Chemnitz and after almost four days without internet connection i have checked my emails and some news sites. I was surprised to read on pro-linux (a German GNU/Linux news site) a comment from a visitor of the CLT that he has talked to us and he thinks that FSFE has a &#8220;very intelligent position on DRM&#8221;. If others has become such a positive impression from FSFE too, i think it was a great success.I had a lot of fun and the CLT was a great event. I hope we will have a booth again next year.

 [1]: http://chemnitzer.linux-tage.de/2007/info/