+++
date = "2019-01-02T00:00:00+02:00"
categories = ["english"]
type = "post"
tags = ["culture", "technology", "software", "kulturtechnik", "FreeSoftware"]
title = "Cultural Techniques"
headerImage = "articles/culture.jpg"
headerImageCopyright = "By Paulis (CC BY-SA 3.0, Wikimedia: https://commons.wikimedia.org/w/index.php?curid=16135556)"
+++

These days I looked up the German word "Kulturtechnik" at [Wikipedia][1] which translates to "cultural techniques" in English. Surprisingly there is no English Wipipedia article for it, so I have to quote the German one. This section attracted my attention the most:

[1]: https://de.wikipedia.org/wiki/Kulturtechnik

> [Für Kulturtechniken] sind ein oder mehrere Voraussetzungen nötig: das Beherrschen von Lesen, Schreiben und Rechnen, die Fähigkeit zur bildlichen Darstellung, analytische Fähigkeiten, die Anwendung von kulturhistorischem Wissen oder die Vernetzung verschiedener Methoden. 
>
> Bei der Entwicklung von Kulturtechniken handelt es sich nicht um Leistungen von Einzelpersonen, sondern um Gruppenleistungen, die in einem soziokulturellen Kontext entstehen. Alle genannten Voraussetzungen benötigen daher immer die soziale Interaktion und gesellschaftliche Teilhabe (Partizipation).

According to this description, cultural techniques are not capabilities of individuals but achievements of a collective, done in a socio-cultural context, it always requires social interaction and participation. This means that reading, writing or math are no culture techniques by itself, but collaborative writing would qualify as such, for example with a wiki or any other collaboration platforms.

## Free Software as a cultural technique

When talking about Free Software I argued already in the past that software is a new cultural technique. My arguments typically was along the line that software is everywhere and shapes our world. Software changed the way we live, learn, work, communicate, participate in society and share our culture. I think that's still true, but this Wikipedia article added an important aspect to me. With the distinction between the tools and what we achieve collectively with it, I think we can argue that software alone is not a cultural technique, but Free Software is.

By definition [Free Software][2] is a licensing model for software. A software license that gives the users the freedom to use, study, share and improve the software makes it Free Software. These days Free Software influence all areas of our live. Cars, airplanes, cash terminals, pay machines, the internet, televisions, smart phones, I could continue the list indefinitely, nothing would be possible without Free Software. The freedom given by the license and the influence it has on all areas of our live changed the way we develop software. A new development model was established and is used for most Free Software these days. The most successful Free Software is developed by open communities with a strong focus on collaboration and participation. This model is embraced by individuals and large organizations. According to the [2017 Linux Kernel Report][3], 4300 developers from over 500 companies have contributed to the kernel, with a impressive list of large companies. Everyone works at the same code, often in parallel. People discuss the changes proposed by each other and improve it together until it is ready to be released. The community is not limited to the code, in a similar collaborative way the corresponding design, documentation, artwork and translations are created. People exchange ideas in online forums, real time chats and meet at conferences. All this happens in a transparent and socio-cultural context, open for everyone to join.

[2]: https://fsfe.org/about/basics/freesoftware
[3]: https://www.linuxfoundation.org/blog/2017/10/2017-linux-kernel-report-highlights-developers-roles-accelerating-pace-change/

But not only the way Free Software is build, also the usage of the software fits the definition of cultural techniques. From a user perspective, Free Software fosters collaboration and participation in many ways. It can be shared freely so that it encourage collaboration in its area of use. For example pupils can exchange the software they use to do their homework or to prepare their presentation. This teaches a culture of collaboration and make sure that everyone has the same possibilities to participate. Different departments in organizations can exchange software and give it to as much employees as needed without worrying that the maximum number of users, allowed by the license is already exceeded.

In a world defined by software, access to software decides who has access to our culture, to our communication tools, about our possibilities in education and at work. Free Software makes sure that everyone has the same possibilities to participate in today's society. It fosters collaboration and participation in contrast to proprietary software which divides people and make sure that everyone is on his own.

All this proves to me that Free Software is the latest cultural technique. As such it requires special attention by policy makers and society. I think it is in all our interest to protect and foster this new cultural technique.
