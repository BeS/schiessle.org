+++
date = "2019-04-05T00:00:00+02:00"
categories = ["english"]
type = "post"
tags = ["nextcloud", "workflow"]
title = "The Power of Workflow Scripts"
headerImage = "articles/gears.jpg"
headerImageCopyright = "By Malcolm Lightbody (Unsplash: https://unsplash.com/photos/401OD83Ke6o)"
+++

[Nextcloud][1] has the ability to define some conditions under which external scripts are executed. The app which makes this possible is called ["Workflow Script"][2]. I always knew that this powerful tool exists, yet I never really had a use case for it. This changed last week.

[1]: https://nextcloud.com
[2]: https://apps.nextcloud.com/apps/workflow_script

## Task

I heavily rely on text files for note taking. I organize them in folders, for example I have a "Projects" folders with sub-folders for each project I work on currently. Within this folders I store all notes, protocols, etc. Each of this project folders contain a "Next Steps.md"-file which contain the next steps in the project. With a growing number of projects, it become harder and harder to keep an overview over all the separated "Next Steps.md"-files. Wouldn't it be nice to have one overview file which contain all the next steps and updates itself automatically when I change one of the individual files?

## Solution

Last week I decided to sit down and write a small bash script which does exactly this:

<pre lang="python">
#!/bin/bash
# call: ./createNextStepsOverview.sh -f <modified-file> -o <owner-of-the-file>

USER=bjoern
NEXTCLOUDDATA=/mnt/nextcloud-data
OVERVIEW_RELATIVE=/$USER/files/Notes/nextcloud/Projects/Overview.md
OVERVIEW=$NEXTCLOUDDATA$OVERVIEW_RELATIVE
PROJECTS=$NEXTCLOUDDATA/$USER/files/Notes/nextcloud/Projects
FILE=''

# Collect Next Steps from the Projects and add it to the overview file
collectNextSteps () {
    PROJECT=$(basename "$1")
    echo $'\n\n'"## $PROJECT" >> $OVERVIEW
    if [ -f "$1"/"Next Steps.md" ]; then
        tail -n +3 "$1"/"Next Steps.md" >> $OVERVIEW
    fi
}

while getopts f:o: option
do
case "${option}"
in
    f) FILE=${OPTARG};;
    o) OWNER=${OPTARG};;
esac
done

# exit when no valid file was given or owner doesn't match the expected user
if [ -z "$FILE" ] || [ "$OWNER" != "$USER" ]; then
    exit 1
fi

# we only update the overview file if the modified "Next Steps.md" was really in a projects folder
if [[ $FILE == *"Notes/nextcloud/Projects/"*"/Next Steps.md" ]]; then
    # Write headline of Overview file
    echo $'Overview\n========' > $OVERVIEW

    # collect all the next steps and write them to the overview file
    find "$PROJECTS" -maxdepth 1 -mindepth 1 -type d | while read f; do collectNextSteps "$f"; done

    # add a footer to the overview file
    echo $'\n\n' >> $OVERVIEW
    echo "Updated: " $(date)$'\n\n' >> $OVERVIEW

    # Rescan overview file
    php /var/www/nextcloud/occ files:scan --path="$OVERVIEW_RELATIVE" &> /dev/null
fi

</pre>

And integrated it into my Nextcloud:

<img src="/img/articles/nc-workflow.png" />

As you can see, I hand over two parameters: The path to the file which was just written, updated or deleted (-f) and the owner of the file (-o)
The script then iterates over all folders in the "Projects" folder, check if there is a "Next Steps.md"-file and writes the content to the Projects/Overview.md file. After the file was written I initiate a files scan in order to update Nextcloud's file cache.

That's it. From now on I just have to go to the Overview.md and see directly all the next steps of all projects I'm working on.

## What's next?

At the moment I manipulate the files directly on the server. This works nicely in my case but it has some limitation, for example in case of a different primary storage it might not be possible to manipulate the files directly on the file system. I discussed the problem with the maintainer of the "Workflow Script"-App. One possible solution could be to generate a temporary app token and handing it over to the script so that files can be manipulated with regular WebDAV calls to the Nextcloud server. I will give it a try during the next days.

I also have already some more scripts in mind. For example, I have a "Archive"-folder to archive finished projects. Currently I think about a script which identifies finished projects by a tag or some keywords and moves them automatically to the archive folder.
