---
title: a lot of impressions and news
author: admin
type: post
date: 2006-11-13T18:57:37+00:00
slug: a-lot-of-impressions-and-news
categories:
  - english
tags:
  - fsfe
  - java
  - FreeSoftware

---
I&#8217;m back from the SFSCon and the first international FSFE Fellowship Meeting in Bolzano (Italy). For me i can say that it was a great event and it gaves me the opportunity to meet a lot of great people from the Free Software Foundation Europe and around Free Software in general.

The Fellowship Meeting was also the place were a new project of the FSFE was launched, the [Freedom Task Force (FTF)][1]. A project which works in partnership with [gpl-violations.org][2] and offers licensing education, fiduciary services and licence enforcement. For more information follow the link above.

But that&#8217;s not enough something more happens. Sun [announced][3] to release Java as [Free Software][4] under the GPL. Already many people have written about it and so i just want to refer to [Georg Greve&#8217;s good analysis][5] of the situation. Also Richard Stallman has already reacted really positive:

> It will be very good that the [Java trap][6] won&#8217;t exist anymore, it will be a thing of the past. That kind of problem can still exist in other areas but it won&#8217;t exist for Java anymore. The GNU general public licence is the most popular and the most widely used software licence, used for some 70% of all free software packages. The special thing about this licence is that it&#8217;s a copyleft licence. That is to say, all versions of the program must carry this licence. So the freedoms that the GNU GPL gives to the users must reach all the users of the program, and that&#8217;s the purpose for which I wrote it. To ensure that all users of the software have the freedom that users should have.
  
> I think Sun has, well, with this contribution, have contributed more than any other company to the free software community in the form of software. And it shows leadership. It&#8217;s an example that I hope others will follow.

And this is the response of Eben Moglen

> As Java became one of the most important languages for the expression of ideas about technology of programming in the last decade the question of Java&#8217;s freedom, wether it could be use freely and made part of free software projects, has been a crucial question. Sun&#8217;s policy of GPL&#8217;ing Java, which we are celebrating now, is an extraordinary achievement in returning programming technology to that state of freely available knowledge. Sun has now GPL&#8217;ed hardware designs, Sun is GPL&#8217;ing Java: that&#8217;s an extraordinary vote of confidence in this way of sharing information. And we, in the free software world, are very pleased and very flattered to see Sun taking its own very valuable and very important product and agreeing with us that they will be more advantageous to Sun as well as to the rest of the community if they are shared under these rules.

So i just have to say thank you and congratulations to Sun for this step.

 [1]: http://www.fsfeurope.org/projects/ftf/ftf.en.html
 [2]: http://www.gpl-violations.org
 [3]: http://www.sun.com/aboutsun/media/presskits/2006-1113/
 [4]: http://fsfeurope.org/documents/freesoftware.en.html
 [5]: https://www.fsfe.org/en/fellows/greve/freedom_bits/free_software_ecosystem_changes_sun_announcing_java_under_gplv2_and_ftf
 [6]: http://www.gnu.org/philosophy/java-trap.html
