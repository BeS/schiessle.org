---
title: “World Intellectual Property Day” – Canadian musicians say “Not in Our Names”
author: admin
type: post
date: 2006-04-27T22:16:34+00:00
slug: world-intellectual-property-day-canadian-musicians-say-not-in-our-names
categories:
  - english
tags:
  - copyright
  - drm

---
April 26th was the [&#8220;World Intellectual Property Day&#8221; (German)][1]. Brigitte Zypries, minister of justice of Germany, said &#8220;We need a better sense of right and wrong for &#8216;intellectual property'&#8221; and announced that the protection of &#8216;intellectual property&#8217; will be the main focus when Germany will held the Presidency of the Council of the European Union in 2007.

At the same time Canadian musicians like Avril Lavigne, Sarah McLachlan or Sloan say [&#8220;Not in Our Names&#8221;][2]. The &#8220;Canadian Music Creators Coalition&#8221; ([CMCC][3]) will ensure that lobbyists for major record labels and music publishers are not the only voices heard in debates about Canada&#8217;s copyright laws and other key cultural policy issues ([press release][4]).

The CMCC is united under three key principles:

  * Suing Our Fans is Destructive and Hypocritical
  * Digital Locks are Risky and Counterproductive
  * Cultural Policy Should Support Actual Canadian Artists

I think this is a great campaign and i hope musicians from other countries will recognize it and follow the Canadian musicians with similar campaigns.<div class="share-on-diaspora" data-url="http://blog.schiessle.org/2006/04/27/world-intellectual-property-day-canadian-musicians-say-not-in-our-names/" data-title=""World Intellectual Property Day" - Canadian musicians say "Not in Our Names"">

 [1]: http://www.heise.de/newsticker/meldung/72409
 [2]: http://www.musiccreators.ca/docs/A_New_Voice-Policy_Paper.pdf
 [3]: http://www.musiccreators.ca/
 [4]: http://www.musiccreators.ca/docs/Press_Release-April_26.pdf
