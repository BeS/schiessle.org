---
title: Login with GnuPG smartcard
author: admin
type: post
date: 2006-08-07T16:09:36+00:00
slug: login-with-gnupg-smartcard
categories:
  - english
tags:
  - gnupg
  - smartcard

---
Libpam-poldi allows you to use your Fellowship crypto card to log in your GNU/Linux system.

First check if poldi detects your cardreader: &#8216;poldi-ctrl -d&#8217;. Unfortunately some cardreader doesn&#8217;t work with poldi and the existing free driver. For example the cardma4040 needs the non-free driver from Omnikey.

If poldi successfully detected your cardreader you can start to configure poldi. Poldi has a pretty good documentation so i will keep my explanations rather short.

  1. Root has to register the new card for poldi: 
    <pre>poldi-ctrl --register-card --account &lt;your-user-account&gt; --serialno &lt;serialno of your card&gt;</pre>
    
    You can also execute this command without &#8216;&#8211;account <your-user-account>&#8217; but than the user will not be able to install or update his card&#8217;s keys.
  
    The serialno can be found by executing &#8216;gpg &#8211;card-status&#8217; and looking for &#8220;Application ID&#8221;.</li> 
    
      * Now we have to establish a mapping between the user and the smartcard he owns: 
        <pre>poldi-ctrl --associate --account &lt;your-user-account&gt; --serialno &lt;serialno of your card&gt;</pre>
    
      * Now you have to write your public key into the appropriate key file (you have to do this within your user account) 
        <pre>poldi-ctrl --set-key</pre>
    
      * That&#8217;s it, now you can test it with &#8216;poldi-ctrl &#8211;test&#8217;
      * Now you have to tell pam, that you want to use poldi.
  
        Therefore you have to edit the files in /etc/pam.d. If, for example, you want to login to kdm with your card, edit the file /etc/pam.d/kdm. Replace the line &#8216;@include common-auth&#8217; with</p> 
        <pre>auth    required   pam_poldi.so</pre>
        
        If you want to login unattended, use
        
        <pre>auth    required   pam_poldi.so try-pin=123456 quiet</pre>
        
        And if you want to fallback to regular unix passwords, use
        
        <pre>auth    sufficient pam_poldi.so try-pin=123456 quietauth    required   pam_unix.so nullok_secure</pre></ol> 
    
    Now you should be able to use your GnuPG smartcard to log in your GNU/Linux system.
