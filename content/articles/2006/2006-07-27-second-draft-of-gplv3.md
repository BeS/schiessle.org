---
title: Second draft of GPLv3
author: admin
type: post
date: 2006-07-27T17:57:04+00:00
slug: second-draft-of-gplv3
categories:
  - english
tags:
  - FreeSoftware
  - gpl

---
After about seven month of discussion and more than 1000 comments through [gplv3.fsf.org/comments/][1] the [FSF][2] has published the second draft of the [GNU General Public License (GPL) Version 3][3] and the first draft of the [GNU Lesser General Public License (LGPL) Version 3][4] which is now designed as a set of permissive exceptions to GPLv3 in accord with section 7. The main changes in the second draft of GPLv3 are clarifications of the DRM section, a reworked license compatibility section and provisions that specifically allow to distribute programs on certain file sharing networks such as BitTorrent. For more details look at <http://gplv3.fsf.org/gpl3-dd2-guide.html>

 [1]: http://gplv3.fsf.org/comments/
 [2]: http://www.fsf.org
 [3]: http://gplv3.fsf.org/gpl-draft-2006-07-27.html
 [4]: http://gplv3.fsf.org/lgpl-draft-2006-07-27.html
