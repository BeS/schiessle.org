---
title: 'Open letter to Bono (U2)  to take a stand against DRM'
author: admin
type: post
date: 2006-06-30T09:12:03+00:00
slug: open-letter-to-bono-u2-to-take-a-stand-against-drm
categories:
  - english
tags:
  - copyright
  - drm

---
[DefectiveByDesign][1], a [FSF][2] campaign to eliminate DRM, has written an open letter to [Bono][3] the lead singer of the Irish rock band [U2][4] to take a stand against Digital Restrictions Management (DRM). The group has focused on Bono because of his social activism and leadership in the music industry. The aim is to collect 10.000 signatures, at which point they will seek an audience with Bono, discuss with him the threats posed by DRM and request that he be the final signer.

Some time ago i have already [blogged][5] about a initiative of Canadian musicians against DRM (<http://www.musiccreators.ca/>). The open letter to Bono could be the first step to achieve something similar in Europe or even worldwide.

You can sign the letter at: <http://defectivebydesign.org/petition/bonopetition>

 [1]: http://defectivebydesign.org
 [2]: http://www.fsf.org
 [3]: http://en.wikipedia.org/wiki/Bono
 [4]: http://en.wikipedia.org/wiki/U2
 [5]: https://www.fsfe.org/en/fellows/schiessle/blog/world_intellectual_property_day_canadian_musicians_say_not_in_our_names