---
title: Test your knowledge of the GPL and LGPL
author: admin
type: post
date: 2006-12-15T18:17:14+00:00
slug: test-your-knowledge-of-the-gpl-and-lgpl
categories:
  - english
tags:
  - FreeSoftware
  - gpl

---
While reading some [FSF Blogs][1] i found a link to a [license quiz][2] where you can test your knowledge of the GPL and LGPL.. If you want to test your knowledge of the GPL and LGPL than try the license quiz.

I remember that i had found this test already some years ago but forgot about it. So i tried it again to see if my licensing knowledge has improved. The last time i did the test i remember that i answered a few questions wrongly but this time everything was correct. Too bad that in a few months we will have a new [GPL and LGPL.][3] 😉

 [1]: http://www.fsf.org/blogs
 [2]: http://www.gnu.org/cgi-bin/license-quiz.cgi
 [3]: http://gplv3.fsf.org/
