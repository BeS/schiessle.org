---
title: Freie Software – Mehr als nur ein Entwicklungsmodell
author: Björn Schießle
type: post
date: 2010-01-30T21:41:55+00:00
slug: freie-software-mehr-als-nur-ein-entwicklungsmodell
categories:
  - deutsch
tags:
  - FreeSoftware
  - fsfe
  - stuttgart
  - slides
image: articles/slides/sofa10/slide1.jpg
summary: In diesem Vortrag auf der Open Fair, eine Parallelveranstaltung zum Weltsozialforum, wurde das Thema erörtert wie Freie Software dazu beitragen kann um mehr Chancengleichheit und Wohlstand in Entwicklungsländern zu schaffen.
location: Stuttgart, Germany
slides: true
headerImage: fsfs.jpg
---
<img src="/img/articles/stuttgart-open-fair-2010.png" class="article-image" style="float: right;" />
**Freie Software &#8211; Mehr als nur ein Entwicklungsmodell**, dass war der Titel meines Vortrags auf der diesjährigen [Stuttgart Open Fair][1] (eine Parallelveranstaltung zum Weltsozialforum) im Rahmen des Workshops &#8220;Gemeingüter zwischen traditionellem Wissen und neuartigen Produktionsweisen&#8221;.

Der Workshop war gut besucht und es ergaben sich während und nach dem Vortrag noch interessante Diskussionen. Dies sind die Folien zu meinem Vortrag:

<div id="praesentation" class="slides">
<ul>
<li><img src="/img/articles/slides/sofa10/slide1.jpg" /></li>
<li><img src="/img/articles/slides/sofa10/slide2.jpg" /></li>
<li><img src="/img/articles/slides/sofa10/slide3.jpg" /></li>
<li><img src="/img/articles/slides/sofa10/slide4.jpg" /></li>
<li><img src="/img/articles/slides/sofa10/slide5.jpg" /></li>
<li><img src="/img/articles/slides/sofa10/slide6.jpg" /></li>
<li><img src="/img/articles/slides/sofa10/slide7.jpg" /></li>
<li><img src="/img/articles/slides/sofa10/slide8.jpg" /></li>
<li><img src="/img/articles/slides/sofa10/slide9.jpg" /></li>
<li><img src="/img/articles/slides/sofa10/slide10.jpg" /></li>
<li><img src="/img/articles/slides/sofa10/slide11.jpg" /></li>
<li><img src="/img/articles/slides/sofa10/slide12.jpg" /></li>
<li><img src="/img/articles/slides/sofa10/slide13.jpg" /></li>
<li><img src="/img/articles/slides/sofa10/slide14.jpg" /></li>
<li><img src="/img/articles/slides/sofa10/slide15.jpg" /></li>
</ul>
<div class="fa fa-arrow-left prevButton button"></div>
<div class="fa fa-arrow-right nextButton button"></div>
</div>

<div class="clear"></div>

Ich habe zum ersten mal einen anderen Ansatz versucht und meine Folien hauptsächlich mit Bildern gestaltet, welche das aktuelle Thema optisch unterstreichen sollten. Nach ersten Rückmeldungen kam dies Art der Präsentation und der Vortrag im allgemeinen gut an.

Da die Folien, durch weniger Text und mehr Bildern, diesmal nicht ganz so aussagekräftig sind, möchte ich an dieser Stelle eine kurze Zusammenfassung des Vortrags geben:

Angefangen habe ich mit der Freie-Software-Definition, um dem sehr gemischten Publikum eine kleine Einführung in Freie Software zu geben. Anschließend habe ich versucht zu veranschaulichen, wie sehr unser Leben heute von Software durchdrungen ist. Eine Interessante Zahl die ich in dem Zusammenhang aus einem Vortrag von [Georg Greve][3] habe: Der Mensch interagiert im Durschnitt pro Tag 300 mal mit Software. Eine erstaunliche Zahl, wie ich finde.
Ausgehend von der Erkenntnis, wie sehr unser Leben heute von Software durchdrungen ist und wie viele unserer täglichen Handlungen von Software abhängig sind habe ich gesellschaftlichen Implikationen von Software, wie Chancengleichheit und Demokratie, angeschnitten. Den Bereich Chancengleichheit habe ich auf zwei Arten veranschaulicht. Als erstes Beispiel diente Freie Software in der Bildung, durch dessen Einsatz alle Schüler die gleichen Chancen im Unterricht haben. Das zweite Beispiel bezog sich auf Entwicklungsländer, denen Freie Software die Möglichkeit bietet am Wissen der Industriestaaten zu lernen um eigenes Wissen und eine eigene Industrie aufzubauen. Zum Bereich Demokratie habe ich ausgeführt, wie Freie Software es ermöglicht die Regeln (&#8220;Gesetze&#8221;) der Informationsgesellschaft zu verstehen und diese als Gesellschaft zu kontrollieren und zu beeinflussen.
  
Abschließend wurden mögliche Entwicklungsmodelle, Geschäftsmodelle auf der Basis von Freier Software beschrieben.

Der Vortrag ging gut 30 Minuten und wurde von anregenden Diskussionen begleitet. Abschließend lässt sich sagen, dass es eine interessante Veranstaltung war, der Workshop hat sehr viel Spaß gemacht und das gemischte Publikum wirkte sehr interessiert.

 [1]: http://www.stuttgart-open-fair.de/
 [3]: http://fsfe.org/about/greve/greve
