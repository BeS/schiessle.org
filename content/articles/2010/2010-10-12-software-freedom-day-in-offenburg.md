---
title: Software Freedom Day in Offenburg
author: Björn Schießle
type: post
date: 2010-10-12T21:34:34+00:00
slug: software-freedom-day-in-offenburg
categories:
  - deutsch
tags:
  - fsfe
  - sfd

---

<img src="/img/articles/sfd2010.jpg" class="article-image" style="float: right; width: 400px" />

Letzten Samstag durfte ich den diesjährigen [Software Freedom Day (SFD)][1] an der FH Offenburg feiern. Organisiert wurde die Veranstaltung von der [LugOG][2] und der [FreieSoftwareOG][3]-Gemeinschaft. Es wurde ein abwechslungsreiches Programm mit zwei Vortragsreihen zusammengestellt. Dabei hatte ich die Ehre die Keynote zu halten. Beim Thema habe ich mich von [Reinhard Müller][4] und dem [&#8220;Mach dich Frei!&#8221;-T-Shirt][5] der FSFE inspirieren lassen. Nach dem Feedback zu urteilen ist der Vortrag sehr gut angekommen, daher werde ich diesen in der einen oder anderen Form sicher wiederverwenden. Die Folien dazu gibt es [hier][6].

Der [Software Freedom Day (SFD)][1] ist eine jährliche, weltweite Veranstaltung. Hierzu werden lokale Veranstaltungen auf der ganzen Welt organisiert um über Freie Software zu informieren. Dieses Jahr wurde der &#8220;Tag der Freien Software&#8221; mit Rund [400 Veranstaltungen][8] gefeiert!

 [1]: http://softwarefreedomday.org/
 [2]: http://www.lugog.de/
 [3]: http://www.freiesoftwareog.org
 [4]: http://wiki.fsfe.org/Fellows/reinhard
 [5]: http://www.fsfe.org/order/2008/tshirt-mdf-green-front-large.jpg
 [6]: http://www.schiessle.org/data/2010-10-09-sfd/mach_dich_frei.pdf
 [7]: http://www.schiessle.org/pics/blog/sfd2010.jpg
 [8]: http://softwarefreedomday.org/map/index.php?year=2010