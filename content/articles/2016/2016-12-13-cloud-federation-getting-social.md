---
title: Cloud Federation – Getting Social
author: Björn Schießle
type: post
date: 2016-12-13T10:45:48+00:00
slug: cloud-federation-getting-social
categories:
  - english
tags:
  - federation
  - nextcloud
  - social
headerImage: articles/nextcloud-user-profile.png
---

With [Nextcloud 11][1] we continue to work on one of our hot topics: Cloud Federation. This time we focus on the social aspects. We want to make it as easy as possible for people to share their contact information. This enabled users to find each other and to start sharing. Therefore we extended the user profile in the personal settings. As the screenshot at the top shows, users can now add a wide range of information to their personal settings and define the visibility for each of them by clicking on the small icon next to it. 

## Privacy first

<img src="/img/articles/nextcloud-user-profile-visibility.png" alt="Change visibility of personal settings" style="float: right" />

We take your privacy serious. That&#8217;s why we provide fine grained options to define the visibility of each personal setting. By default all new settings will be private and all settings which already exists before will have the same visibility as on Nextcloud 10 and earlier. This means that the users full name and avatar will only be visible to users on the same Nextcloud server, e.g. through the share dialog. If enabled by the administrator, this values, together with the users email address, will be synced with trusted servers to allow users from trusted servers to share with each other seamlessly.

As shown at the screenshot at the right we provide three levels of visibility: &#8220;Private&#8221;, &#8220;Contacts&#8221; and &#8220;Public&#8221;. Private settings will be only visible to you, even users on the same server will not have access to it. The only exceptions are the avatar and the full name because this are central data used at Nextcloud for activities, internal shares, etc. Settings which are set to &#8220;Contacts&#8221; will be shared with users on the same server and trusted servers, defined by the administrator of the Nextcloud server. Public data will be synced to a global and public address book. 

## Introducing the global address book

The best real world equivalent to the global address book is a telephone directory. For a new phone number people can chose to publish their phone number together with their name and address to a public telephone directory to enable other people to find them. The global address book follows the same pattern. By default nothing gets published to the global address book. Only if the user sets at least one value in their personal settings to &#8220;Public&#8221;. In this case all the public data will be synced to the global address book together with the users Federated Cloud ID. Users can remove their data at any time again by simply setting their personal data back to &#8220;Contacts&#8221; or &#8220;Private&#8221;.

In order to use the global address book as a source to find new people, this lookup needs to be enabled explicitly in the &#8220;Federated Cloud Sharing&#8221; settings by the administrator. For privacy reasons this is disabled by default. If enabled the share dialog of Nextcloud will query the global address book every time a user wants to share a file or folder, and suggest people found in the global address book. In the future there might be a dedicated button to access the global address book, both for performance reasons and to make the feature more discoverable.

### Future work

The global address book can return many results for a given name. How do we know that we share with the right person? Therefore we want to add the possibility to verify the users email address, website and Twitter handle in Nextcloud 12. As soon as this feature is implemented the global address book will only return users where at least one personal setting is verified and also visualize the verified data so that the user can use this information to pick the right person.

Further, I want to extend the meaning of &#8220;Contacts&#8221; in one of the next versions. The idea is that &#8220;Contacts&#8221; should not be limited to trusted servers but include the users personal contacts. For example the data set to &#8220;Contacts&#8221; could be shared with every person to which the user already established at least one federated share successfully, or to all contacts with a Federated Cloud ID in the users personal address book. This way we will move slowly in the direction of some kind of decentralized and federated social network based on the users address book. This will also enable users to easily push their new phone number or other personal data to all their friends and colleagues, things for which most people use centralized and proprietary services like so called &#8220;business networks&#8221; these days.

Another interesting possibility, made possible by the global address book is to move complete user accounts from one server to another. Given that the user published at least some basic information on the global address book, they could use it to announce their move to another server. Other Nextcloud servers could find this information and make sure that existing federated shares continue to work.

 [1]: https://nextcloud.com/blog/nextloud-11-sets-new-standard-for-security-and-scalability/
