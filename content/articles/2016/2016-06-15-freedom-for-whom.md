---
title: Freedom for whom?
author: Björn Schießle
type: post
date: 2016-06-15T20:34:22+00:00
slug: freedom-for-whom
categories:
  - English
tags:
  - business
  - cla
  - FreeSoftware
  - nextcloud
headerImage: articles/we-want-freedom.jpg
headerImageCopyright: By Quinn Dombrowski (CC BY-SA 2.0)

---

This discussion is really old. Since the first days of the Free Software movement people like to debate to whom the freedom in Free Software is directed? The users? The code? The developers? Often this goes along with a discussion about [copyleft][1] vs non-protecting Free Software licenses like the BSD- and the MIT-License. I don&#8217;t want to repeat this discussion but look at the question from a complete different angle. I want to look at it from the position of a software company and its business model.

If you talk to Free Software companies you realize, that very few have a business model completely based on Free Software. Most companies add proprietary extensions on top and use this as the main incentive for customers to buy their software. In 2008 Andrew Lampitt coined the term [open core][2] to describe this kind of business models. There are many ways to argue in favor of open core. One argument I hear quite often is that the proprietary parts are only useful for large enterprises, so nothing is taken away from the community. This way the community gets reduced to the typical home user, which is a interesting way of looking at it. Why should we make such a distinction? And why does home users deserve software freedom more then large organizations?

I understand that freedom in the context of software is a concept which can sound scary to some companies at the beginning. After all, that was the main reason why Open Source was invented, a [marketing campaign for Free Software][3] to make business people feel more comfortable. Interestingly this changes quickly if you go into more details about what software freedom really means. More entrepreneurial freedom, control over the tools they use, software freedom as a precondition for privacy and security, independence, freedom to chose the supplier with the best offering and in case of software development the freedom to build on existing, well established technology instead of building everything from scratch. These are freedoms well understood and appreciated by entrepreneurs and they demand it in many other areas of their daily business. This lead me to the conclusion that software freedom is not only something for home users but it also important for large organizations.

Open core often comes with a important side-effects. Most companies pick a strong copyleft license like the GNU GPL or the GNU AGPL, and then demand that every contributor signs a Contributor License Agreement (CLA). This CLA puts the company in a strong position. They are the only one who can distribute the software under a proprietary license and add proprietary extensions. This effectively removes one of the biggest strengths of copyleft licenses. If you set CLAs aside, copyleft licenses are a great tool to create an ecosystem of equal participants. Equality is really important to make individuals and organizations feel confident that joining the initiative is worthwhile in the long term. Everybody having the same rights and the same duties is the only way to develop a strong ecosystem with many participants. Therefore it is no wonder that projects using CLAs often get slowed down and have a less diverse community.

RedHat was one of the first company which understood that all this, CLA&#8217;s and proprietary extensions, do more harm than good. It slows down the development. It keeps your community smaller as necessary and it adds the burden to develop all the proprietary extensions by your own instead of leveraging the power of a large community which can consists of employees, hobbyists, partners and customers. This goes so far that [RedHat even embrace competitors like CentOS][4], which basically gives RedHat Enterprise Linux away for free to people who don&#8217;t need the support. For a truly open organization this is not a problem but a great opportunity to spread the software and to become more popular. That&#8217;s a key factor to make sure, that RedHat is the de facto standard if it comes to enterprise GNU/Linux distributions.

If a initiative is driven by a strong company it can be useful to move some parts out to a neutral entity. RedHat did this by founding the [Fedora project][5]. Another way to do this is by creating a foundation which makes sure that everyone has the same rights. Such a foundation should hold all rights necessary to make sure the project can continue no matter what happens to individual participants, including companies. For the governance of such a foundation it is important that it is not controlled by a single entity.

This is exactly what makes me feel so excited about what we are doing at Nextcloud. We are building a complete free cloud solutions, not only for home users but for everyone. This solution will be much more than just file sync and share, from a company point of view stuff like calendar, contacts and video conferencing will become a first class citizen. All this will be Free Software, developed together with a great community. Home users, customers and partners are invited to be part of it, not just as a consumer but as part of a large and diverse community. Everybody should be empowered to change things to the better. In order to make all this independent from a single company we will set up a foundation. As described above the foundation will make sure that we have a intact and growing ecosystem with no single point of failure. This guarantees that Nextcloud can survive us and any other participant if needed.

 [1]: https://www.gnu.org/copyleft/
 [2]: https://en.wikipedia.org/wiki/Open_core
 [3]: https://web.archive.org/web/20021217003716/http://www.opensource.org/advocacy/faq.html
 [4]: http://readwrite.com/2013/08/13/red-hat-ceo-centos-open-source/
 [5]: https://getfedora.org/
