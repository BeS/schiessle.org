---
title: Keynote at Open16 - Restore the Internet
author: Björn Schießle
type: post
date: 2016-09-29T08:02:32+00:00
categories:
  - english
tags:
  - nextcloud
  - federation
  - slides
slides: true
image: articles/slides/open16/slide1.jpg
summary: Keynote at the Open16 on how Nextcloud can help to restore a free, decentralized and open Internet.
location: Open16 in Mechelen, Bergium
headerImage: nextcloud.png
---
This year I was invented to the Open16 in Mechelen (Belgium) to give a keynote.
It was a really nice event with many interesting presentation and talks.

I was asked by the organizer to started the keynote with the history of Nextcloud. 
Why did we started Nextcloud and what do we want to do different. Then I moved on
with the main topic: "Restore the Internet - free, decentralized, open". Why it is 
important and what we are doing at Nextcloud to make it happen. This are the slides
to my talk:

<div id="keynote" class="slides">
<ul>
<li><img src="/img/articles/slides/open16/slide1.jpg" alt="" /></li>
<li><img src="/img/articles/slides/open16/slide2.jpg" alt="˘ˇˆ˙˝˛ ˜ ˙! ˜!˜" $˜%&'% (*$+  , . $$$, !!!.) $, -!!! " /></li>
<li><img src="/img/articles/slides/open16/slide3.jpg" alt="/˝012$3 " /></li>
<li><img src="/img/articles/slides/open16/slide4.jpg" alt="˝ %&'' " /></li>
<li><img src="/img/articles/slides/open16/slide5.jpg" alt="/˝ 4$ !$.4 6* ˝7˚7 " /></li>
<li><img src="/img/articles/slides/open16/slide6.jpg" alt="* 8! ˙0 ˙ 5 + " /></li>
<li><img src="/img/articles/slides/open16/slide7.jpg" alt="0!˜9˜˜˘:6˙.&; " /></li>
<li><img src="/img/articles/slides/open16/slide8.jpg" alt="<˝ ˙>˚ ˙ !A@˘767! ,0B " /></li>
<li><img src="/img/articles/slides/open16/slide9.jpg" alt="˙!˚ 0$˝ + !˝˝ ! !!) !! (!!!!!! !!!!01 !˝ " /></li>
<li><img src="/img/articles/slides/open16/slide10.jpg" alt="* 00˘9˜˜˘:.&; " /></li>
<li><img src="/img/articles/slides/open16/slide11.jpg" alt="/.. ˝*$. /˝!. ˘!!0. " /></li>
<li><img src="/img/articles/slides/open16/slide12.jpg" alt="/.. ˝*$. /˝!. ˘!!0. " /></li>
<li><img src="/img/articles/slides/open16/slide13.jpg" alt="/.. ˝*$. /˝!. ˘!!0. " /></li>
<li><img src="/img/articles/slides/open16/slide14.jpg" alt="<˝$˝0 " /></li>
<li><img src="/img/articles/slides/open16/slide15.jpg" alt="" /></li>
<li><img src="/img/articles/slides/open16/slide16.jpg" alt="<˝5 0D˙@˚˘1˜˜˜˘E.&; " /></li>
<li><img src="/img/articles/slides/open16/slide17.jpg" alt="<˝5<0 0?˜˜˘:6˙.&; " /></li>
<li><img src="/img/articles/slides/open16/slide18.jpg" alt=" 3 ˙$?! 2 ˜  <. /˝˝! !3 ?)!˚ " /></li>
<li><img src="/img/articles/slides/open16/slide19.jpg" alt="7 *$G!*$ :?$$0! :$˝!!! G! $! " /></li>
<li><img src="/img/articles/slides/open16/slide20.jpg" alt="/˝ H$˝ H$˝! ˜* ! " /></li>
<li><img src="/img/articles/slides/open16/slide21.jpg" alt="/˝ " /></li>
<li><img src="/img/articles/slides/open16/slide22.jpg" alt=" !# %&'I +*˚J< !! 7* !˝*!˝ !! D ! " /></li>
<li><img src="/img/articles/slides/open16/slide23.jpg" alt="˙˝! ˙˝? "!˝ ˝ "+ !˝* $ $!$˝ !!˝! " /></li>
<li><img src="/img/articles/slides/open16/slide24.jpg" alt="5!!! !!. (?!!, M-L!+M " /></li>
<li><img src="/img/articles/slides/open16/slide25.jpg" alt="˙˝? 8!˝!˝6 *! "˝  !˝  ! " /></li>
<li><img src="/img/articles/slides/open16/slide26.jpg" alt="< <!!+ ˝! 0 <!!+   0  * !! " /></li>
<li><img src="/img/articles/slides/open16/slide27.jpg" alt="/˝ $70 " /></li>
<li><img src="/img/articles/slides/open16/slide28.jpg" alt="/˝ !C ?!C " /></li>
<li><img src="/img/articles/slides/open16/slide29.jpg" alt="*$˝˝ C!1! ˙!!!!+! ˙!?!!!! !+! ˜$!!!+! 9˙. " /></li>
<li><img src="/img/articles/slides/open16/slide30.jpg" alt="? " /></li>
<li><img src="/img/articles/slides/open16/slide31.jpg" alt="775* ˚ !! ˚ !! ˚ !!˘ N % O E 7 ! ˝ 7 ! ˝ 7 ! ˝ 7 ! ˝ 7 ! ˝ " /></li>
<li><img src="/img/articles/slides/open16/slide32.jpg" alt="775* ˚ !! ˚ !! ˚ !!˘ N % O E ! ? * P # ˝ Q " /></li>
<li><img src="/img/articles/slides/open16/slide33.jpg" alt="775* ˚ !! ˚ !! ˚ !!˘ N % O E " , ˇ ˝ - N -  .  . . . " /></li>
<li><img src="/img/articles/slides/open16/slide34.jpg" alt="775* ˚ !! ˚ !! ˚ !!˘ N % O E ˙ ˝ $ ˝ , ˇ ˝ - N " /></li>
<li><img src="/img/articles/slides/open16/slide35.jpg" alt="  / 1? ˜?!0C  $ !!+6 " /></li>
<li><img src="/img/articles/slides/open16/slide36.jpg" alt="" /></li>
</ul>
<div class="fa fa-arrow-left prevButton button"></div>
<div class="fa fa-arrow-right nextButton button"></div>
</div>

