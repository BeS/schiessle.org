---
title: 140 Zeichen – Eine Menschenrechtsverletzung?
author: Björn Schießle
type: post
date: 2012-10-30T17:31:25+00:00
slug: 140-zeichen-eine-menschenrechtsverletzung
categories:
  - deutsch
tags:
  - grüne
  - netzpolitik
  - piraten
  - policy

---
Gerade habe ich einen Bericht auf [Zeit-Online][1] über eine Matinee des Zeit-Verlages mit dem Titel &#8220;Demokratie 2.0&#8221; gelesen. Hierbei trafen Claudia Roth von Bündnis90/Die Grünen und Bernd Schlömer von der Piratenpartei zum ersten mal aufeinander. Während der Diskussion antwortete Frau Roth auf die Frage, ob sie denn auch twittere mit &#8220;Nein, denn für mich ist es fast eine Menschenrechtsverletzung, immer nur mit 140 Zeichen zu kommunizieren&#8221;.

Auch wenn es Frau Roth in dieser Situation vermutlich gar nicht bewusst war, so hat sie dennoch eine sehr wichtige Erkenntnis formuliert die weit über Twitter und dessen Zeichenbegrenzung hinaus geht. Internet und Computer bieten eine Unmenge von neuen Möglichkeiten. Sie revolutionieren die Art wie wir kommunizieren, lernen und arbeiten. Wir müssen aber aufpassen wer diese Medien kontrolliert und damit die Regel aufstellt, nach denen wir in Zukunft diesen Tätigkeiten nachgehen. Denn mit der Kontrolle dieser neuen Medien erhält man auch die Entscheidungshoheit darüber, wer mit wem in welcher Form kommuniziert kann, was unser Computer können und nicht zuletzt auch über den Zugang zu unseren Daten. Im Fall von Twitter sind das die zitierten 140 Zeichen und egal wie sehr jemand will oder wie dringlich es erscheint eine ausführlichere Antwort zu formulieren, die Regeln stehen fest und lassen keine Ausnahme zu.

Beim lesen des Berichts ist mir auch ein Beispiel von Lawrence Lessig aus seinem Buch [&#8220;Code and other laws of Cyberspace&#8221;][2] wieder eingefallen. Hier beschreibt er Chat-Räume bei AOL (American Online) die auf 23 Personen begrenzt sind. Aber warum genau 23? Warum nicht 22? Oder 24? Könnte die Anzahl der Teilnehmer nicht auch offen bleiben? Mit solchen Entscheidungen kann man sehr genau festlegen welche Möglichkeiten der Diskussion, der Teilhabe und der Verbreitung von Information möglich sind. Solche Entscheidungen können ein Werkzeug sehr mächtig und nützlich machen oder so stark einschränken, dass man es kaum noch sinnvoll nutzen kann. Die Macht die von der Möglichkeit ausgeht solche Regeln festzulegen ist enorm.

Unsere Kommunikation hängt heute immer mehr von Computern, dem Internet und damit in letzter Konsequenz von Software ab. Wer immer diese Software kontrolliert entscheidet über Zugang, Form und Möglichkeiten der Kommunikation. Wir als Gesellschaft sollten diese Kontrolle nicht einzelnen Unternehmen überlassen. Die Kontrolle über diese zentrale Infrastruktur der Informationsgesellschaft gehört in die Hände der Gesellschaft und wir sollten bei der Wahl unsere Werkzeuge sehr genau darauf achten wer diese eigentlich kontrolliert. Nur so können wir gesellschaftlichen Werte wie Chancengleichheit, Demokratie und auch die von Frau Roth richtig herangezogenen Menschenrechte im digitalen Zeitalter wahren. Dies ist keine graue Theorie, es gibt bereits Projekte die sich genau dies zum Ziel gesetzt haben, der Gesellschaft wieder die Kontrolle über ihr (digitales) Leben zurück zu geben. Angefangen von Computer-Systemen wie [GNU/Linux][3] über freie und dezentrale alternativen zu Twitter wie [Status.Net][4], dezentralen und freien [sozialen Netzwerken][5], bis hin zu freien sogenannten &#8220;Cloud&#8221; Lösungen, zum Beispiel in der Form von [ownCloud][6].

Wir haben die Möglichkeit und die Verantwortung uns die richtigen Werkzeuge auszuwählen, mit denen wir uns in der digitalen Welt bewegen. Ich bin fest davon überzeugt, dass wir unsere hart erkämpften Werte nur dann in das Informationszeitalter übertragen können wenn wir darauf achten, dass unsere Werkzeuge alle drei Anforderungen erfüllen:

  * [Freie Software][7], so dass jeder die Software verstehen, anwenden, teilen und anpassen kann.
  * [Offene Standards][8] zusammen mit dezentralen Strukturen um uns unabhängig miteinander zu vernetzen und auszutauschen
  * Die volle [Kontrolle über unsere Daten][9] die wir Online bereitstellen, teilen und archivieren

Wenn wir alle zusammen darauf achten, dann kann sich Frau Roth zusammen mit uns allen ganz frei in der digitalen Welt bewegen, an Diskussionen und Entscheidungen teilnehmen und, um den Kreis zu schließen, dabei völlig frei entscheiden wie viel Zeichen sie dafür benötigt.

 [1]: http://www.zeit.de/politik/deutschland/2012-10/schloemer-roth-treffen
 [2]: http://www.code-is-law.org "Code is Law"
 [3]: http://www.gnu.org
 [4]: http://www.status.net
 [5]: http://www.socialswarm.net
 [6]: http://www.owncloud.org
 [7]: https://fsfe.org/about/basics/freesoftware
 [8]: https://fsfe.org/activities/os/def
 [9]: http://userdatamanifesto.org/