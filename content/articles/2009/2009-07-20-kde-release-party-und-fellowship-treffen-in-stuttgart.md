---
title: KDE Release-Party und Fellowship-Treffen in Stuttgart
author: Björn Schießle
type: post
date: 2009-07-20T09:27:27+00:00
slug: kde-release-party-und-fellowship-treffen-in-stuttgart
categories:
  - deutsch
tags:
  - fsfe
  - kde
  - stuttgart

---
Es wird mal wieder Zeit für ein [Fellowship][1]-Treffen in Stuttgart!

Vor einem halben Jahr haben wir uns mit einigen Leuten von KDE in Stuttgart getroffen, um zusammen mit euch die Veröffentlichung von KDE 4.2 zu [feiern][2]. Dieses Treffen war ein solcher Erfolg, dass wir euch pünktlich zur Veröffentlichung von KDE 4.3 zu einer weiteren gemeinsamen KDE Release-Party mit Fellowship-Treffen einladen wollen.

Ihr hattet schon immer Fragen, die ihr jemanden von der [FSFE][4] oder von [KDE][3] persönlich stellen wolltet? Ihr wollt euch einfach in gemütlicher Runde mit der Stuttgarter Freie-Software-Gemeinschaft austauschen oder in Zukunft vielleicht sogar selber aktiv werden? Das ist die Gelegenheit!

Wir treffen uns hierzu am 1. August um 19 Uhr im Restaurant Wartburg Tol(l)eranz, Gutenbergstrasse 87, 70197 Stuttgart (West). Weitere Informationen findet ihr wie immer in unserem [Wiki][5].

Eigeladen sind alle, die sich für Freie Software, die FSFE oder KDE interessieren.

Um die Anzahl der Leute besser abschätzen zu können, würde ich mich über eine kurze Mail von euch freuen.

 [1]: http://fellowship.fsfe.org
 [2]: http://wiki.fsfe.org/groups/Stuttgart/KDE_4_2_Release_Party
 [3]: http://www.kde.org
 [4]: http://www.fsfe.org
 [5]: http://wiki.fsfe.org/groups/Stuttgart