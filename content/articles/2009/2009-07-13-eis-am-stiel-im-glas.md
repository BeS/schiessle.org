---
title: “Eis am Stiel im Glas”
author: Björn Schießle
type: post
date: 2009-07-13T17:43:57+00:00
slug: eis-am-stiel-im-glas
categories:
  - deutsch
tags:
  - fsfe
  - fun

---
Heute habe ich mich mit Giacomo Poderi und Matthias Kirschner in Waiblingen getroffen, um uns über ein paar aktuelle Themen Rund um Freie Software und der FSFE auszutauschen. Unter anderem haben wir uns über bevorstehende Veranstaltungen, das Fellowship und [&#8220;Windows Tax Refund&#8221;][1] unterhalten.

Aber das interessanteste an diesem Mittag enteckten wir auf der Speisekarte: &#8220;Eis am Stiel im Glas&#8221;. Matthias wollte es schließlich wissen und wir waren alle gespannt, was sich hinter dieser Bezeichnung wohl verstecken würde. Schon die etwas komische Reaktion der Bedienung, als wir die Bestellung aufgaben, hätte uns nachdenklich stimmen sollen. Aber seht selbst, was am Ende auf unserem Tisch stand:

<img src="/img/articles/eis-am-stiel-im-glas.png" alt="Eis am Stiel im Glas" class="article-image center" />

&#8230;Ehrlich gesagt hatte ich ja schon etwas kreativeres erwartet&#8230; 😉<div class="share-on-diaspora" data-url="http://blog.schiessle.org/2009/07/13/eis-am-stiel-im-glas/" data-title=""Eis am Stiel im Glas"">

 [1]: http://wiki.fsfe.org/Windows-Tax_Refund
 [2]: https://blog.schiessle.org/wp-content/plugins/wp-freedom-share/diaspora/img/diaspora-share.png