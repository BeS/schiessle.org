---
title: A successful year for the 1. RFC Stuttgart
author: Björn Schießle
type: post
date: 2009-07-08T16:29:19+00:00
slug: a-successful-year-for-the-1-rfc-stuttgart
categories:
  - english
tags:
  - robocup
  - stuttgart
  - uni

---
This year it was the first time I participated at RoboCup tournaments. It was quite stressful but also really exiting and most important: successful!

In April the year started with the German Open in Hanover. It was a hard week, we lived in a bus and worked every day far into the night. But it was worthwhile. After many years of struggle the [1. RFC Stuttgart][1] finale made it and win the German Open!

Here you can see a summary of the final game in Hanover against Osnabrück (Stuttgart = cyan; Osnabrück = magenta):

<iframe width="560" height="315" src="https://www.youtube.com/embed/Su1ArYhltfc" frameborder="0" allowfullscreen></iframe>

But this was not the end of our success in 2009. Last week we went to Graz, Austria for the RoboCup world championship. Like a few weeks ago in Hanover it was again a hard week. At the end we went home with two cups! First we won the free challenge with the presentation of our &#8220;Automatic Camera Man&#8221; and at Sunday we finally won the final and became world champion for the first time!
  
<img class="article-image" style="float: right; width: 200px" src="/img/articles/robocup09_result.png" />
  
The tournament started quite well with 6:0 wins in the first round robin. In the second round robin we had our only defeat and finished the round with 3:1 wins. The third round robin ended with 2:0 wins. In the semifinal we won against MRL (Iran) 4:1. Than in the final we met Tech United (Eindhoven, Netherlands) and beat them 4:1 for the world championship.

This is the team which made all this happen:
  
<img class="article-image center" style="width: 400px;" src="/img/articles/robocup09_team.png" />

Below you can see a video, recorded by Tech United, from the final (Stuttgart = magenta; Tech United = cyan).

<iframe width="560" height="315" src="https://www.youtube.com/embed/Pb6OB3OJbmI" frameborder="0" allowfullscreen></iframe>

By the way, all robots are powered by Debian GNU/Linux and the software is developed with C++ and Qt (for graphical tools)&#8230;

 [1]: http://robocup.informatik.uni-stuttgart.de
 [2]: http://www.youtube.com/watch?v=Su1ArYhltfc
 [3]: http://www.youtube.com/watch?v=Pb6OB3OJbmI