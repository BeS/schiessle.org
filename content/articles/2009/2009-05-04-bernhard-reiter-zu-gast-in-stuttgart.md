---
title: Bernhard Reiter zu Gast in Stuttgart
author: Björn Schießle
type: post
date: 2009-05-04T14:33:47+00:00
slug: bernhard-reiter-zu-gast-in-stuttgart
categories:
  - deutsch
tags:
  - fsfe
  - stuttgart
  - uni

---
Vom 6. bis 7. Mai wird Bernhard Reiter, Deutschland-Koordinator der [FSFE][1], zu Gast in Stuttgart sein. Wir wollen die Gelegenheit für ein spontanes Fellowship-Treffen nutzen. Das Treffen wird wie immer im Unithekle in Stuttgart/Vaihingen stattfinden. Eine genaue Uhrzeit wird auf der Wiki Seite der [Fellowship-Gruppe Stuttgart][2] noch bekannt gegeben. Wie immer sind alle Fellows und Freie Software interessierte eingeladen daran teilzunehmen.

Am Donnerstag, den 7. Mai wird Bernhard Reiter dann an der Universität Stuttgart (Campus Vaihingen) einen Gastvortrag im Rahmen der Vorlesung [&#8220;Free/Libre and Open Source Software Engineering&#8221;][3] halten. Der Titel seines Vortrags lautet &#8220;Professionelle Freie Software in Wirtschaft und Politik&#8221;.

In dem Vortrag wird es u.a. um Folgende Themen gehen:

  * [Freie Software][4], was ist das?
  * Eine handvoll Lizenzkategorien reicht aus
  * Freie Software und Politik, viele Chancen noch offen
  * Flurschaden in der gesamten Softwarebranche durch [Patente][5]
  * Streit um [offene Standards][6]
  * offene Entwicklung ist nicht gleichzusetzen mit Freie Software
  * Tipps vom Freie Software Profi

Natürlich wird auch genug Zeit für Diskussionen bleiben. Die Teilnahme steht für jeden offen und ist kostenfrei. Der Vortrag beginnt um 11:30Uhr und wird im Hörsaal V55.01 stattfinden.

 [1]: http://www.fsfe.org
 [2]: http://wiki.fsfe.org/groups/Stuttgart
 [3]: http://www.iits.uni-stuttgart.de/education/courses/teaching_summer_term_2009/floss/index.html
 [4]: http://www.fsfe.org/documents/freesoftware.de.html
 [5]: http://www.fsfe.org/projects/swpat/swpat.de.html
 [6]: http://www.fsfe.org/projects/os/os.de.html