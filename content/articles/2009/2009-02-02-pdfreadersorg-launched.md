---
title: PDFreaders.org launched
author: Björn Schießle
type: post
date: 2009-02-02T22:18:10+00:00
slug: pdfreadersorg-launched
categories:
  - english
tags:
  - fsfe
  - OpenStandards

---
Today the Free Software Foundation Europe (FSFE) [announced][2] their latest initiative: [PDFreaders.org][1]. The initiative started in the Fellowship and is coordinated by the Fellows Hannes Hauswedell and Jan-Hendrik Peters. The Fellowship increases FSFE&#8217;s financial independence, FSFE&#8217;s political weight, FSFE&#8217;s workforce and is the source of many great projects, activites and campaigns. Read more about the [Fellowship][3].

Many websites with PDF documents have a link to a proprietary PDF reader from one specific company. This initiative tries to change this and offers buttons to link to pdfreaders.org which leads people to a list of [free as in freedom][4] and vendor neutral PDF readers.

FSFE president Georg Greve explains: &#8220;Interoperability, competition and choice are primary benefits of [Open Standards][5] that translate into vendor-independence and better value for money for customers. Although many versions of PDF offer all these benefits for formatted text and documents, files in PDF formats typically come with information that users need to use a specific product. pdfreaders.org provides an alternative to highlight the strengths of PDF as an Open Standard.&#8221;

Jan-Hendrik Peters, one of the coordinators of this initiative says: &#8220;Free Software gives us control over the software we use, and Open Standards give us control over our data and allow implementations by many different groups. We wanted to show that with the Portable Document Format people can have both.&#8221;

Support Free Software and Open Standards! Put one of the [PDFreaders.org-Buttons][6] next to PDF files on your websites!

 [1]: http://www.pdfreaders.org
 [2]: http://www.fsfeurope.org/news/2009/news-20090202-01.en.html
 [3]: http://fellowship.fsfe.org
 [4]: http://www.fsfeurope.org/documents/freesoftware.en.html
 [5]: http://www.fsfeurope.org/projects/os/
 [6]: http://www.pdfreaders.org/graphics.html
