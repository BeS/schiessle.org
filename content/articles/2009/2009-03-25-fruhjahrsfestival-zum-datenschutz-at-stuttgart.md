---
title: “Frühjahrsfestival zum Datenschutz” at Stuttgart
author: Björn Schießle
type: post
date: 2009-03-25T10:19:58+00:00
slug: fruhjahrsfestival-zum-datenschutz-at-stuttgart
categories:
  - english
tags:
  - cccs
  - fsfe
  - FreeSoftware
  - privacy
  - stuttgart
  - slides
image: articles/slides/ccc09/slide1.jpg
summary: Freie Software alleine garantiert keine sichere Software, aber sie ist eine notwendige Grundvoraussetzung um überhaupt sichere und vertrauenswürdige Software zu schaffen.
location: Stuttgart, Germany
slides: true
headerImage: fsfs.jpg
---
<img src="/img/articles/fruehjahrsfestival-datenschutz-stuttgart-2009.jpg" alt="Frühlingsfestival zum Datenschutz" title="Frühlingsfestival zum Datenschutz" style="float: left; width=300px" class="article-image" /> Today the [&#8220;Frühjahrsfestival zum Datenschutz&#8221;][1] (engl. &#8220;spring festival of privacy&#8221;) starts at the public library of Stuttgart. This will be a 4 day event from 25. to 28. March 2009.

Today the event will start at 19:30 with a speech by Padeluun, he is a member of the [Big Brother Award][2] panel in Germany and a chairman of the [FoeBuD e.V.][3], an association which works for civil rights and privacy. The title of his talk will be &#8220;Datenkraken. Die Kommunikationsgesellschaft und ihre Feinde&#8221; (engl: The communication-society and their enemies). Don&#8217;t miss it!

The next days there will be a lot of workshops for pupils, parents and teenagers and other interesting talks.

The last day (Saturday, 28. March) will be organised by the [Chaos Computer Club Stuttgart (CCCS)][4] with a lot of interesting [speeches][5] and practical examples how to improve privacy. At 14:00 I will give a speech about &#8220;Freie Software und Datenschutz&#8221; (engl: Privacy and Free Software). This are the slides to my talk:

<div id ="praesentation" class="slides">
<ul>
<li><img src="/img/articles/slides/ccc09/slide1.jpg" alt="FreieSoftwareundDatenschutz Bj ornSchieˇle <schiessle@fsfeurope.org> FreeSoftwareFoundationEurope(FSFE) 28.M arz2009 " /></li>
<li><img src="/img/articles/slides/ccc09/slide2.jpg" alt="Inhalt WashatSoftwaremitDatenschutzzutun? WasistFreieSoftware? WasbringtFreieSoftwaref urdenDatenschutz? PraktischeBeispiele Zusammenfassung " /></li>
<li><img src="/img/articles/slides/ccc09/slide3.jpg" alt="WashatSoftwaremitDatenschutzzutun? I Softwareist uberall I normalerMenschinteragiertimSchnittmehrals300malam TagmitSoftware! I Kommunikation,privateDaten,...vielesl auftheute uber SoftwareundwirdmitderHilfevonSoftwareverwaltet, abgespieltusw. " /></li>
<li><img src="/img/articles/slides/ccc09/slide4.jpg" alt="SoftwarefehlermitAuswirkungenaufden Datenschutz/Sicherheit I NetscapesSSLwardieerstenJahreunsicher I SoftwareerzeugteunsicherePINsf urBankkarten I Fr uhereWindowsVersionenenthieltNSASchl ussel I OpenSSLBuginDebianGNU/Linux I Audio-undVideoplayerprotokollierenabgespielte Musik-/Videotitel " /></li>
<li><img src="/img/articles/slides/ccc09/slide5.jpg" alt="WasistFreieSoftware? 1. DieFreiheit,dasProgrammf urjedenZweckauszuf uhren. 2. DieFreiheit,dieFunktionsweiseeinesProgrammszu untersuchen,undesanseineBed urfnisseanzupassen. 3. DieFreiheit,Kopienweiterzugeben. 4. DieFreiheit,einProgrammzuverbessern,unddie Verbesserungenandie eitweiterzugeben. I Rechte,keine I DasGegenteilvonFreierSoftwareistpropriet areSoftware. I FreieSoftwareundkommerzielleSoftwaresindkein Widerspruch! " /></li>
<li><img src="/img/articles/slides/ccc09/slide6.jpg" alt="WasistFreieSoftware? 1. DieFreiheit,dasProgrammf urjedenZweckauszuf uhren. 2. DieFreiheit,dieFunktionsweiseeinesProgrammszu untersuchen,undesanseineBed urfnisseanzupassen. 3. DieFreiheit,Kopienweiterzugeben. 4. DieFreiheit,einProgrammzuverbessern,unddie Verbesserungenandie eitweiterzugeben. I Rechte,keine I DasGegenteilvonFreierSoftwareistpropriet areSoftware. I FreieSoftwareundkommerzielleSoftwaresindkein Widerspruch! " /></li>
<li><img src="/img/articles/slides/ccc09/slide7.jpg" alt="Einschub:DerQuelltext-oderdasKochrezept Bin ar 01001100101010 00100000100010 01110010000010 00100011100101 10101000100011 00001100000111 11001011001010 11001010111000 00101010010101 Quelltext #include<stdio.h> intmain(void) { printf("HalloStuttgart!"); return0; } Quellcodeerm oglichtdaslesen/schreiben imInformationszeitalter " /></li>
<li><img src="/img/articles/slides/ccc09/slide8.jpg" alt="Einschub:DerQuelltext-oderdasKochrezept Bin ar 01001100101010 00100000100010 01110010000010 00100011100101 10101000100011 00001100000111 11001011001010 11001010111000 00101010010101 Quelltext #include<stdio.h> intmain(void) { printf("HalloStuttgart!"); return0; } Quellcodeerm oglichtdaslesen/schreiben imInformationszeitalter " /></li>
<li><img src="/img/articles/slides/ccc09/slide9.jpg" alt="WasistFreieSoftware? 1. DieFreiheit,dasProgrammf urjedenZweckauszuf uhren. 2. DieFreiheit,dieFunktionsweiseeinesProgrammszu untersuchen,undesanseineBed urfnisseanzupassen. 3. DieFreiheit,Kopienweiterzugeben. 4. DieFreiheit,einProgrammzuverbessern,unddie Verbesserungenandie eitweiterzugeben. I Rechte,keine I DasGegenteilvonFreierSoftwareistpropriet areSoftware. I FreieSoftwareundkommerzielleSoftwaresindkein Widerspruch! " /></li>
<li><img src="/img/articles/slides/ccc09/slide10.jpg" alt="WasbringtFreieSoftwaref urdenDatenschutz? " /></li>
<li><img src="/img/articles/slides/ccc09/slide11.jpg" alt="VerteilungvsKonzentrationderMacht UnfreieSoftware: I NurwenigekennendasInnenlebenderSoftware I NurwenigeentscheidenwelcheFunktionenindieSoftware reinkommen I Machtistkonzentriert! FreieSoftware: I Jederkann\in"dieSoftwarereinschauen I (Oft)groˇeEntwicklergemeinschaftmitbreitem Interessensspektrum I Machtistverteilt! " /></li>
<li><img src="/img/articles/slides/ccc09/slide12.jpg" alt="AuswirkungenaufdenDatenschutz I Ichkannmichdavon Uberzeugen,dassdasProgrammnurdas machtwasichwill I Wennunerw unschtesVerhaltenentdecktwird,kannichesan derWurzelabstellen I NeueVersionkannver werdenundkommtdamit jedemzugute " /></li>
<li><img src="/img/articles/slides/ccc09/slide13.jpg" alt="Browser:Firefox Abbildung: http://www.firefox.com " /></li>
<li><img src="/img/articles/slides/ccc09/slide14.jpg" alt="E-Mail:Thunderbird Abbildung: http://www.mozillamessaging.com " /></li>
<li><img src="/img/articles/slides/ccc09/slide15.jpg" alt="InstantMessaging:Pidgin Abbildung: http://www.pidgin.im " /></li>
<li><img src="/img/articles/slides/ccc09/slide16.jpg" alt="Audio-/Videoplayer:VLC Abbildung: www.videolan.org/vlc " /></li>
<li><img src="/img/articles/slides/ccc09/slide17.jpg" alt="Op Abbildung: http://www.openoffice.org " /></li>
<li><img src="/img/articles/slides/ccc09/slide18.jpg" alt="PDF-Betrachter:Okular,Sumatra,Skim,... Abbildung: http://www.pdfreaders.org " /></li>
<li><img src="/img/articles/slides/ccc09/slide19.jpg" alt="EinkomplettesBetriebssystem:GNU/Linux Abbildung: KDEundGNOMEDesktop " /></li>
<li><img src="/img/articles/slides/ccc09/slide20.jpg" alt="Zusammenfassung(1) FreieSoftwarebietetdieFreiheitder... 1. ...Nutzung 2. ...Studium 3. ...Weitergabe 4. ...Verbesserung LassenSiesichdieFreiheitenunddieKontrolle uberIhreneigeneComputernichtnehmen! " /></li>
<li><img src="/img/articles/slides/ccc09/slide21.jpg" alt="Zusammenfassung(2) WasbringtFreieSoftwaref urdenDatenschutz? 1. Uneingeschr ankteKontrolle uberdieSoftware! 2. KritischesVerhaltenkanndabehobenwerdenwoesauftritt... 3. ...undmitanderengeteiltwerden Esgibtf urfastalleBereicheFreieSoftwareAlternativen! NutzenSiedieM oglichkeiten! " /></li>
<li><img src="/img/articles/slides/ccc09/slide22.jpg" alt="DANKE! " /></li>
</ul>
<div class="fa fa-arrow-left prevButton button"></div>
<div class="fa fa-arrow-right nextButton button"></div>
</div>

Don&#8217;t miss the event! I&#8217;m sure it will be great!

 [1]: http://www5.stuttgart.de/stadtbuecherei/MeineDaten/
 [2]: http://www.bigbrotherawards.org/
 [3]: http://www.foebud.org/
 [4]: http://www.cccs.de
 [5]: http://www.cccs.de/wiki/bin/view/Main/StadtBuecherei200903
 [6]: https://blog.schiessle.org/wp-content/plugins/wp-freedom-share/diaspora/img/diaspora-share.png
