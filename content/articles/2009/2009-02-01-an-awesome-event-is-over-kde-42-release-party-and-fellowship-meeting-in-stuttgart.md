---
title: 'An awesome event is over: KDE 4.2 Release Party and Fellowship Meeting at Stuttgart'
author: Björn Schießle
type: post
date: 2009-02-01T16:30:33+00:00
slug: an-awesome-event-is-over-kde-42-release-party-and-fellowship-meeting-in-stuttgart
categories:
  - english
tags:
  - fsfe
  - kde
  - stuttgart

---
At Friday, 30.Jan.09 we had a joint event between [KDE][1] and the [Fellowship][2] of the [Free Software Foundation Europe (FSFE)][3] and it was simply awesome! Beside a lot of KDE and FSFE people we had a lot of visitors and at the end we were about 40 people! All interested in KDE 4.2 and [Free Software][4].

[<img src="/img/articles/fsfe-meeting-stuttgart-2009-1.jpg" class="article-image" style="float: left; width: 300px" />][5]

At the beginning Frederik started with an introduction and showed all the new and cool things in KDE 4.2. After that i gave a talk about the work of FSFE, especially in 2008, explained the role of the Fellowship for FSFE and highlighted some cool activities from our Fellows. My slides can be found [here][6] (German). I think i could attract some people and would be really happy to see some new faces at our next Fellowship meeting (probably somewhen in March). If you don&#8217;t want to miss it, subscribe to our [mailing list for South Germany][7].

[<img src="/img/articles/fsfe-meeting-stuttgart-2009-2.jpg" class="article-image" style="float: right; width: 300px" />][8]

After my talk we made a small break and than Frank continued with a very interesting talk about the KDE community and how to get involved as a developer, writer, artist or user. Last but not least Sven talked about the new version of Amarok and what we can expect from the new release.

This was the official part. Now the party started and we had a lot of fun and many interesting discussions between KDE developers and users, FSFE Fellows and all the other people who joined us at this evening. We also discussed some possible activities for the [Fellowship Group Stuttgart][9]. Some Fellows seems to be quite interested in the topic &#8220;Free Software in school and education&#8221;. I also think that this is a really important topic. Remember, the pupils from today are the decision makers from tomorrow.

[<img src="/img/articles/fsfe-meeting-stuttgart-2009-3.jpg" class="article-image" style="float: left; width: 300px" />][10]

As it becomes later a small group of people survived. One of them was Martin Konold, a KDE Developer from the very first days of KDE. He told us a lot of interesting and funny stories about the beginning of KDE and the development of the Free Software desktop.

At 2:30 at night a great event was over. I really want to thank [Frederik][11] for his great help in organising this event and all the other KDE people who helped to make this event that awesome! It was a lot of fun and a great cooperation between KDE and FSFE people! Looking forward for the next opportunity to organise such a great event!

More pictures can be found [here][12].

 [1]: http://www.kde.org
 [2]: http://fellowship.fsfe.org
 [3]: http://www.fsfeurope.org
 [4]: http://www.fsfeurope.org/documents/freesoftware.en.html
 [5]: https://wiki.fsfe.org/FellowshipGroup/Stuttgart/KDE_4_2_Release_Party?action=AttachFile&do=view&target=img_0025.jpg
 [6]: http://www.schiessle.org/data/2009-01-30-fellowsip/fsfe+fellowship.pdf
 [7]: https://lists.fsfe.org/mailman/listinfo/sueddeutschland
 [8]: https://wiki.fsfe.org/FellowshipGroup/Stuttgart/KDE_4_2_Release_Party?action=AttachFile&do=view&target=img_0033.jpg
 [9]: https://wiki.fsfe.org/FellowshipGroup/Stuttgart/
 [10]: https://wiki.fsfe.org/FellowshipGroup/Stuttgart/KDE_4_2_Release_Party?action=AttachFile&do=view&target=img_0035.jpg
 [11]: http://www.kdedevelopers.org/blog/4326
 [12]: https://wiki.fsfe.org/FellowshipGroup/Stuttgart/KDE_4_2_Release_Party