+++
date = "2018-10-17T21:00:00+02:00"
categories = ["deutsch"]
type = "post"
tags = ["fsfe", "FreeSoftware", "slides"]
image = "articles/slides/htw-dresden/slide1.jpg"
title = "Eine Einführung in Freie Software"
summary = "Im Wintersemester 2018 organisiert die Hochschulgruppe \"Freie Software und Freies Wissen Dresden\" die Ringvorlesung \"Freie Software und Freies Wissen als Beruf\" in dessen Rahmen ich einen Einführungsvortrag zu Freier Software gehalten habe."
location = "HTW Dresden, Deutschland"
slides = "true"
headerImage = "articles/slides/htw-dresden/htw-dresden.jpg"
headerImageCopyright = "By Björn Schießle (CC BY-SA)"
+++

In diesem Wintersemester organisiert die Hochschulgruppe ["Freie Software und Freies Wissen Dresden"](https://fsfw-dresden.de) die Ringvorlesung ["Freie Software und Freies Wissen als Beruf"](https://wiki.fsfw-dresden.de/doku.php/ringvorlesung/ws2018) an der HTW Dresden. Im Rahmen dieser Vorlesung wurde ich eingeladen eine Einführung in die Geschichte und Philosophie der Freien Software zu halten. Obwohl die Vorlesung zu später Stunde von 17:00 bis 18:30 stattfand war der Hörsaal gut gefüllt. Schon während des Vortrags wurden viele interessierte Fragen gestellt, welche im Anschluss noch zu weiteren anregenden Diskussionen geführt haben. 

Freie Software und Freies Wissen spielen in Gesellschaft, Politik und Wirtschaft eine immer zentralere Rolle. Daher würde ich mir wünschen, dass es solche Vortragsreihen regelmäßig an allen Hochschulen gibt.

Wie man an der [Themenliste](https://wiki.fsfw-dresden.de/doku.php/ringvorlesung/ws2018) sieht, gibt es noch viele weitere spannende Vorträge von bekannten Vertretern aus der Freie Software und Freie Wissen Community welche man auf keinen Fall verpassen sollte.


<div id="htw-dresden"  class="slides">
<ul>
<li><img src="/img/articles/slides/htw-dresden/slide1.jpg" alt="slide0" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide2.jpg" alt="slide1" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide3.jpg" alt="slide2" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide4.jpg" alt="slide3" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide5.jpg" alt="slide4" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide6.jpg" alt="slide5" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide7.jpg" alt="slide6" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide8.jpg" alt="slide7" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide9.jpg" alt="slide8" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide10.jpg" alt="slide9" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide11.jpg" alt="slide10" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide12.jpg" alt="slide11" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide13.jpg" alt="slide12" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide14.jpg" alt="slide13" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide15.jpg" alt="slide14" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide16.jpg" alt="slide15" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide17.jpg" alt="slide16" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide18.jpg" alt="slide17" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide19.jpg" alt="slide18" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide20.jpg" alt="slide19" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide21.jpg" alt="slide20" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide22.jpg" alt="slide21" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide23.jpg" alt="slide22" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide24.jpg" alt="slide23" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide25.jpg" alt="slide24" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide26.jpg" alt="slide25" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide27.jpg" alt="slide26" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide28.jpg" alt="slide27" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide29.jpg" alt="slide28" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide30.jpg" alt="slide29" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide31.jpg" alt="slide30" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide32.jpg" alt="slide31" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide33.jpg" alt="slide32" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide34.jpg" alt="slide33" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide35.jpg" alt="slide34" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide36.jpg" alt="slide35" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide37.jpg" alt="slide36" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide38.jpg" alt="slide37" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide39.jpg" alt="slide38" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide40.jpg" alt="slide39" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide41.jpg" alt="slide40" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide42.jpg" alt="slide41" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide43.jpg" alt="slide42" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide44.jpg" alt="slide43" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide45.jpg" alt="slide44" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide46.jpg" alt="slide45" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide47.jpg" alt="slide46" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide48.jpg" alt="slide47" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide49.jpg" alt="slide48" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide50.jpg" alt="slide49" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide51.jpg" alt="slide50" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide52.jpg" alt="slide51" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide53.jpg" alt="slide52" /></li>
<li><img src="/img/articles/slides/htw-dresden/slide54.jpg" alt="slide53" /></li>
</ul>
<div class="fa fa-arrow-left prevButton button"></div>
<div class="fa fa-arrow-right nextButton button"></div>
</div>

