+++
date = "2018-11-01T19:00:00+02:00"
categories = ["english"]
type = "post"
tags = ["zim", "productivity"]
title = "Keep yourself organized"
image = "articles/keep-yourself-organized-zim.png"
+++

Over the years I tried various tools to organize my daily work and manage my ToDo list, including Emacs [org-mode](https://orgmode.org/), [ToDo.txt](http://todotxt.org/), [Kanban boards](https://apps.nextcloud.com/apps/deck) and simple plain text files. This are all great tools but I was never completely happy with it, over time it always became to unstructured and crowded or I didn't managed to integrate it into my daily workflow.

Another tool I used through all this time was [Zim](http://zim-wiki.org/), a wiki-like desktop app with many great features and extensions. I used it for notes and to plan and organize larger projects. At some point I had the idea to try to use it as well for my task management and to organize my daily work. The great strength of Zim, it's flexibility can be also a weak spot because you have to find your own way to organize and structure your stuff. After reading various articles like ["Getting Things Done" with Zim](http://zim-wiki.org/manual/Usage/Getting_Things_Done.html) and trying different approaches I come up with a setup which works nicely for me.

## Basic setup

Zim allows you to have multiple, completely separated "notebooks". But I realized soon that this adds another level of complexity and makes it less likely for me to use it regularly. Therefore I decided to just have one single notebook, with the three top-level pages "FSFE", "Nextcloud" and "Personal". Each of this top-level pages have four sub-pages "Archive", "Notes", "Ideas" and "Projects". Every project which is done or notes which are no longer needed will be moved to the archive.

Additionally I created a top-level category called "Journal". I use this to organize my week and to prepare my weekly work report. Zim has a quite powerful journal functionality with a integrated calendar. You can have daily, weekly, monthly or yearly journals. I decided to use weekly journals because at Nextcloud we also write weekly work reports, so this fits quite well. Whenever I click on a date in the calendar the journal of the corresponding week opens. I use a template so that a newly created journal looks like the screenshot at the top.

## Daily usage

As you can see at the picture at the top, the template adds directly the recurring tasks like "check support tickets" or write "work report". If one of the task is completed I tick the checkbox. During a day typically tasks come in which I can't handle directly. In this case I will quickly decide to add it to the current day or the next day, depending on how urgent the task is and whether I plan to handle it at the same day or not. Sometimes, if I already know that the task is for a deadline later during the week it can happen that I add it directly to a later day, in rare situations even to the next week. But in general I try to plan the work not to far in the future. 

In the past I added everything to my ToDo list, also stuff for which the deadline was a few months away or stuff I wanted to do once I have some time left. My experience is that this grows the list so quickly that it becomes highly unproductive. That's why I decided to add stuff like this to the "Ideas" category. I review this category regularly and see if some of this stuff could become a real project, task or if it is no longer relevant. If it is something which has already a deadline, but only in a few months, I create a calendar entry with a reminder and add it later to the task list once it become relevant again. This keeps the ToDo list manageable. 

At the bottom of the screenshot you see the beginning of my weekly work report, so I can update it directly during the week and don't have to remember at the end of the week what I have actually done. At the end of each day I review the tasks for the day, mark finished stuff as done and re-evaluate the other stuff: Some tasks might be no longer relevant so that I can remove them completely, otherwise I move them forward to the next day. At the end of the week the list looks something like this:

![Zim Journal on Wednesday](/img/articles/keep-yourself-organized-zim-example.png "Zim Journal on Wednesday")

This is now the first time were I start to feel really comfortable with my workflow. The first thing I do in the morning is to open Zim, move it to my second screen and there it stays open until the end of the day. I think the reasons that it works so well is that it provides a clear structure, and it integrates well with all the other stuff like notes, project handling, writing my work report and so on.

## What I'm missing

Of course nothing is perfect. This is also true for Zim. The one thing I really miss is a good mobile app to add, update or remove task while I'm away from my desktop. At the moment I sync the Zim notebook with Nextcloud and use the Nextcloud Android app to browse the notebook and open the files with a normal text editor. This works, but a dedicated Zim app for Android would make it perfect.
