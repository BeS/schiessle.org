---
title: Vortrag an der Universität Stuttgart
author: admin
type: post
date: 2008-01-31T22:28:23+00:00
slug: vortrag-an-der-universitat-stuttgart
categories:
  - deutsch
tags:
  - fsfe
  - stuttgart
  - uni

---
Gestern (am 30. Januar 2008) habe ich an der Universität Stuttgart im Rahmen der [inf.misc][1] Reihe einen Vortrag über Freie Software gehalten. Inf.misc ist ein Veranstaltung der Fachschaft Informatik und Softwaretechnik, die von Studenten für Studenten gehalten wird. Dabei geht es um Themen, die von offiziellen Veranstaltungen der Fakultät nicht gelehrt werden.

Thema des Vortrags war die Bedeutung von Software in der Informationsgesellschaft. Neben Freier Software im allgemeinen habe ich einen Überblick über die verschiedenen Lizenzmodelle in der Freien Software Welt gegeben und die 3. Version der GNU General Public License (GPL) sowie die [FSFE][2] vorgestellt.

Die Folien des Vortrags können [hier][3] heruntergeladen werden.

 [1]: http://fachschaft.informatik.uni-stuttgart.de/studium/infmisc/index.php
 [2]: http://www.fsfeurope.org
 [3]: http://www.schiessle.org/data/freie_software.pdf