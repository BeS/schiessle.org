---
title: 'Rolle Rückwärts in München: Diese Fragen sollte man sich stellen'
author: Björn Schießle
type: post
date: 2017-02-14T11:08:50+00:00
slug: rolle-ruckwarts-in-munchen-diese-fragen-sollte-man-sich-stellen
image: articles/limux-infographic.png
categories:
  - deutsch
tags:
  - limux
  - münchen
  - fsfe
  - policy
---
<img src="/img/articles/limux.png" width="300" style="float: right" />

Bereits diesen Mittwoch soll die Entscheidung fallen: München will bis 2021 die Nutzung des GNU/Linux-Desktop-Clients &#8220;LiMux&#8221; beenden und damit ein erfolgreiches Leuchtturmprojekt beenden. Ich habe dazu einen ausführlichen [Artikel auf Netzpolitik.org veröffentlicht][1]. Bürger haben ein Recht darauf, dass der Stadtrat verantwortungsvoll und professionell arbeitet. Angesichts der Tragweite der Entscheidung sollte auf jede der folgenden Fragen den beteiligten Stadträten die Antwort daher bereits vorliegen. Gerne dürfen diese Fragen auch als Grundlage verwendendet werden um die Entscheidungsträger zu kontaktieren.

## Die drängendsten Fragen zur Entscheidung über die zukünftige IT Startegie in München

  * Warum ignoriert der Stadtrat mit seiner Entscheidung die von ihm selbst beauftragten Studien (MitKonkret, it@M Plus, Accenture) welche alle übereinstimmend mit der it@M die Organisationsstrukturen als Ursprung aller Probleme erkannt haben?
  * Existiert ein Zusammenhang zwischen dem Umzug von Microsoft nach München und der Rückkehr zu Microsoft?
  * Wieso werden immer wieder kostenintensive Gutachten und externe Firmen beansprucht, wenn der Stadtrat letztendlich diesen Empfehlungen nicht folgt? Siehe u.a. auch bereits Mitkonkret, die damals eine volle GmbH oder ein IT-Referat empfohlen haben aber von einer Mischform abrieten.
  * Auf wessen Kompetenz fundiert der Stadtratsbeschluss und die darin enthaltene Einschätzung dass Accenture, it@M und alle beteiligten IT-Experten falsch liegen und die Ursachen sich mit einer technologischen Entscheidung lösen ließen?
  * Diese Entscheidung des Stadtrates hat tiefgreifende technische Konsequenzen. Welcher IT-Architekt hat den Stadtrat diesbezüglich beraten und entschieden, dass die kostenintensive Umstellung auf Microsoft Windows eine notwendige Maßnahme ist?
  * Wie glaubt der Stadtrat in zwei Jahren eine komplett neue Infrastruktur zu schaffen angesichts der Tatsache dass auch die bestehende Infrastruktur mit der geplanten Grösse ihre Schwierigkeiten hat. Wie sollen in diesem Zeitraum 30.000 Mitarbeiter geschult, über 9.000 Vorlagen umgebaut, Wollmux und hunderte von komplexen Makros ersetzt werden?
  * Ist dem Stadtrat bewusst, wie er durch den Antrag die Leistung der IT-Mitarbeiter der Stadt herabwürdigt und dass dieses Verhalten die Attraktivität der Landeshauptstadt München als Arbeitgeber für kompetente IT-Spezialisten nachhaltig schädigt?
  * Was passiert mit den Mitarbeitern, die zur Zeit für den Basisclient und LibreOffice entwickeln? Werden diese entlassen oder wird ihnen eine angemessene Zeit zur Umschulung gegeben?
  * Welche Auswirkung hat diese Entscheidung auf den Betrieb der GNU/Linux-Server und das erst kürzlich neu aufgebaute Rechenzentrum der Stadt?

## Weiter Fragen die man sich zu dem Thema stellen sollte

<img src="/img/articles/limux-infographic.png" width="212" style="float: right" />

  * Wieso glaubt der Stadtrat an, Fachentscheidungen besser treffen zu können als die mit dem Thema beauftragten Fachleute?
  * Haben die Stadträte des Antrags geschäftliche Verbindungen zu Microsoft oder Microsoft-nahen Firmen?
  * Auf welcher Basis wurde entschieden, dass Windows das geeignete System sei?
  * Die Entscheidung ist nicht produktneutral und benachteiligt u.a. deutsche Anbieter und den Wirtschaftsstandort München. Ist das legal oder drohen der Stadt und den für die Entscheidung Verantwortlichen rechtliche Konsequenzen und Regressforderungen?
  * Erratische und unabgestimmte Entscheidungen können auch dazu führen dass viele Firmen bei Ausschreibungen der Landeshauptstadt München in Zukunft nicht mehr teilnehmen. Wie will der Stadtrat dies verhindern wenn Ausschreibungen, Projekte und betriebsfähige Software durch diese Entscheidung einfach weggeworfen werden?
  * Soll die technische Umstellung die notwendige administrative Umstellung ersetzen?
  * Wie verhält sich die geplante Entscheidung zum Teil der Entscheidungsvorlage in der die Notwendigkeit einer heterogenen, von Windows unabhängigen, Infrastrukturstrategie hervorgehoben wird?
  * Wie verhält sich diese Entscheidung insbesondere zu der &#8220;Web First&#8221;-Strategie, welche ebenfalls in der Entscheidung angesprochen wird?
  * Es gab schon in der Vergangenheit Gerüchte, dass der Stadtrat sich nicht an die Vorgaben der Stadt gehalten hat und unbetreute Windowsclients im Backbone der Stadt oder für Stadtratsaufgaben eingesetzt hat. Dies wäre u.A. eine grob fahrlässige Gefährdung der IT-Sicherheit. Entsprechen diese Gerüchte den Tatsachen?
  * Worauf begründet der Stadtrat seinen Optimismus, dass es keine Reibungsverluste zwischen den DiKas, der GmbH und dem Eigenbetrieb geben wird?
  * Soll der Stadtratsbeschluss zu einer Abschaffung der städtischen Infrastruktur führen bei der alle Daten der Bürger in der Accenture/Microsoft-Cloud gespeichert werden?
  * Wie sieht der Stadtrat die Gewährleistung des Datenschutzes, insbesondere angesichts der Executive Order von Präsident Trump welche den Datenschutz für nicht-US-Bürger abgeschafft hat.
  * Der Stadtrat zwingt mit dieser Entscheidung die Bürger der Landeshauptstadt München zum Einsatz von Microsoft Office für die Kommunikation mit der Verwaltung. Übernimmt der Stadtrat auch die Kostenfolgen dieser Entscheidung für die Bürger die aktuell nicht über Microsoft Office verfügen?
  * Der Stadtrat hat verlauten lassen, dass der Dokumentenaustausch wegen mangelnder Kompatibilität problematisch sei. Auf welche Art und Weise hat der Stadtrat vor, den Austausch von Dokumenten per E-Mail auszubauen? Angesichts von Viren, Erpressungs-Crypto-Trojanern und staatlichen Angreifern mit dem Ziel der Wirtschaftsspionage und
    
    Manipulation scheint dieses Ziel in die falsche Richtung zu gehen. Stellt dies nicht eine Gefährdung der Sicherheit der städtischen IT durch die Entscheidung des Stadtrats dar?
  * Wie beeinflusst die Entscheidung des Stadtrats die Kosteneffizenz des Gutachtens?

 [1]: https://netzpolitik.org//2017/kommentar-die-abkehr-von-freier-und-unabhaengiger-software-in-muenchen-waere-falsch/
 [2]: https://blog.schiessle.org/wp-content/uploads/LiMuxinfographicv3-de.png
