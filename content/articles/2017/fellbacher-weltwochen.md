+++
date = "2017-09-22T22:44:52+02:00"
categories = ["deutsch"]
type = "post"
tags = ["FreeSoftware", "fsfe", "slides"]
image = "articles/slides/fellbacherWeltwochen2017/slide1.jpg"
title = "Freie Software auf den Fellbacher Weltwochen"
summary = "Wie kann man mit Freier Software nachhaltig für eine gute Bildung und eine starke und unabhängige Wirtschaft sorgen."
location = "Fellbacher Weltwochen, Germany"
slides = "true"
headerImage = "fsfs.jpg"
+++

Am 20. Oktober werde ich auf den [Fellbacher Weltwochen](http://weltwochen.ichbinbaff.de/) zum Thema Freie Software sprechen. Der Titel meines Vortrags lautet "Digitale Abhängigkeit vermeiden - Mit Freier Software für gute Bildung und eine starke und unabhängigen Wirtschaft sorgen" und darum geht es:

[<img src="/img/articles/fellbacher-weltwochen-2017.jpg" width="150" style="float: right; margin-left: 10px;" />](/img/articles/fellbacher-weltwochen-2017.jpg)

> Software ist heute aus dem Alltag nicht mehr weg zu denken. Laut einer
> Studie interagieren wir im Schnitt mehr als 300 mal am Tag mit
> Software. Damit nimmt Software eine Schlüsselrolle in unserem Leben
> ein. Sie entscheidet über unsere Möglichkeiten am kulturellen Leben
> teilzunehmen, über unsere Bildung und unsere Möglichkeiten am
> Arbeitsmarkt. Proprietäre Software, also Software welche als eine Art
> Black-Box ausgeliefert wird, macht es unmöglich diese zu verstehen und
> eigene Lösungen darauf aufzubauen. Niemand außer dem Hersteller,
> welcher in der Regel aus einem reichen westlichen Land kommt, ist in
> der Lage Fehler zu beheben oder die Software an lokale Gegebenheiten
> anzupassen. Mit dem Export dieser Software in wirtschaftlich schwächere
> Länder laufen wir Gefahr neue, diesmal digitale, Kolonien zu errichten
> indem wir den Menschen zwar auf den ersten Blick nützliche Werkzeuge in
> die Hand geben, sie aber gleichzeitig abhängig von wenigen großen
> Unternehmen machen. Damit nehmen wir den Menschen jegliche Möglichkeit
> diese neue Kulturtechnik zu verstehen und für sich zu nutzen. Freie
> Software, also Software die jeder verwenden, studieren, anpassen und
> weitergeben kann, bietet hier einen Ausweg. Mit Freier Software
> exportieren wir nicht nur Werkzeuge sondern Wissen, was die Basis für
> gute Bildung, Unabhängigkeit und nicht zuletzt für eine starke lokale
> Wirtschaft bildet.

Für alle die nicht vorbeischauen können und zum nachlesen, habe ich hier schon mal meine Präsentation:

<div id="fellbacherWeltwochen2017"  class="slides">
<ul>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide1.jpg" alt="slide0" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide2.jpg" alt="slide1" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide3.jpg" alt="slide2" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide4.jpg" alt="slide3" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide5.jpg" alt="slide4" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide6.jpg" alt="slide5" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide7.jpg" alt="slide6" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide8.jpg" alt="slide7" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide9.jpg" alt="slide8" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide10.jpg" alt="slide9" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide11.jpg" alt="slide10" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide12.jpg" alt="slide11" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide13.jpg" alt="slide12" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide14.jpg" alt="slide13" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide15.jpg" alt="slide14" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide16.jpg" alt="slide15" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide17.jpg" alt="slide16" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide18.jpg" alt="slide17" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide19.jpg" alt="slide18" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide20.jpg" alt="slide19" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide21.jpg" alt="slide20" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide22.jpg" alt="slide21" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide23.jpg" alt="slide22" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide24.jpg" alt="slide23" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide25.jpg" alt="slide24" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide26.jpg" alt="slide25" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide27.jpg" alt="slide26" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide28.jpg" alt="slide27" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide29.jpg" alt="slide28" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide30.jpg" alt="slide29" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide31.jpg" alt="slide30" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide32.jpg" alt="slide31" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide33.jpg" alt="slide32" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide34.jpg" alt="slide33" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide35.jpg" alt="slide34" /></li>
<li><img src="/img/articles/slides/fellbacherWeltwochen2017/slide36.jpg" alt="slide35" /></li>
</ul>
<div class="fa fa-arrow-left prevButton button"></div>
<div class="fa fa-arrow-right nextButton button"></div>
</div>

Interessant? Dann freue ich mich auf euren Besuch und spannende Diskussionen am 20. Oktober in der Volkshochschule Fellbach, Theodor-Heuss-Str. 18, im Raum Nummer 07. Einlass 18:30 Uhr.
