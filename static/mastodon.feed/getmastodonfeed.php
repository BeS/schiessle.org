<?php
/**
 * @copyright Copyright (c) 2017 Bjoern Schiessle <bjoern@schiessle.org>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

require_once 'Mastodon_api.php';
require_once '/home/schiesbn/mastodon.feed/config.php';

class GetMastodonFeed {

    /** @var \Mastodon_api */
    private $api;

    private $cacheFile = 'myStatusCache.json';

    /** @var int keep cache at least 300 seconds = 5 minutes */
    private $threshold = 300;

    /** @var string url of the mastodon instance */
    private $mastodonUrl = 'https://mastodon.social';

    /** @var string token to authenticate at the mastodon instance */
    private $bearerToken;

    /** @var string uid on the mastodon instance */
    private $uid;

    public function __construct($config) {

        $this->mastodonUrl = $config['mastodon-instance'];
        $this->bearerToken = $config['token'];
        $this->uid = $config['user-id'];
        $this->threshold = $config['threshold'];

        $this->api = new Mastodon_api();

        $this->api->set_url($this->mastodonUrl);
        $this->api->set_token($this->bearerToken, 'bearer');
    }

    public function getOwnStatuses() {
        $cachedStatuses = $this->getCachedStatuses();
        if (!empty($cachedStatuses)) {
            $timestamp = (int)key($cachedStatuses);
            $currentTime = time();
            if ($timestamp + $this->threshold < $currentTime) {
                return $this->fetchOwnStatuses();
            }
            return reset($cachedStatuses);
        }

        return $this->fetchOwnStatuses();
    }

    /**
     * fetch own statuses from Mastodon and update the cache file
     *
     * @return array
     */
    private function fetchOwnStatuses() {
        $statuses = $this->api->accounts_own_statuses( $this->uid);
        if (isset($statuses['html']) && count($statuses['html']) > 0 ) {
            $statuses = $this->formatResult($statuses);
            $this->updateCache($statuses);
            return $statuses;
        }
        return [];
    }

    /**
     * format the result, trip html tags from toot and unnecessary values
     *
     * @param $statuses
     * @return mixed
     */
    private function formatResult($statuses) {
        $result = [['content' => 'Many small people who in many small places do many small things that can alter the face of the world', 'url' => '']];
        foreach ($statuses['html'] as $key => $status) {
            if ($status['visibility'] !== 'public') continue;
            if ($status['in_reply_to_id'] !== null) continue;
            $result[] = [
                'content' => strip_tags($status['content']),
                'url' => $status['url']
            ];
        }

        return $result;
    }


    /**
     * read cached statuses from file
     *
     * @return array
     */
    private function getCachedStatuses() {
        if (file_exists($this->cacheFile)) {
            $cachedStatues = file_get_contents($this->cacheFile);
            $cachedStatusArray = json_decode($cachedStatues, true);
            if (is_array($cachedStatusArray)) {
                return $cachedStatusArray;
            }
        }

        return [];
    }

    /**
     * update status cache file
     *
     * @param array $statuses
     */
    private function updateCache(array $statuses) {
        $timestamp = time();
        $cache = [$timestamp => $statuses];
        file_put_contents($this->cacheFile, json_encode($cache));
    }

}

$mastodon = new GetMastodonFeed($config);
$statuses = $mastodon->getOwnStatuses();


// headers for not caching the results
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

// headers to tell that result is JSON
header('Content-type: application/json');

// send the result now
echo json_encode($statuses);
